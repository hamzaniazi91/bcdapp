// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova' ,'ionic.contrib.drawer','ngAnimate'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
      $ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

    

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html',
          controller: 'BrowseCtrl'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })
    .state('app.rental', {
      url: '/rental',
      views: {
        'menuContent': {
          templateUrl: 'templates/rental.html',
          controller: 'rentalCtrl'
        }
      }
    })

    .state('app.driver', {
      url: '/driver',
      views: {
        'menuContent': {
          templateUrl: 'templates/fulltimeDriver.html',
          controller: 'driverCtrl'
        }
      }
    })
   .state('app.info', {
      url: '/info',
      views: {
        'menuContent': {
          templateUrl: 'templates/clientInfo.html',
          controller: 'infoCtrl'
        }
      }
    })

 .state('app.req', {
      url: '/req',
      views: {
        'menuContent': {
          templateUrl: 'templates/requirements.html',
          controller: 'reqCtrl'
        }
      }
    })

    .state('landing', {
      cache: false,
      url: '/landing',
      templateUrl: 'templates/landing.html',
      controller: 'landingCtrl'
        
    })

    .state('signup', {
      url: '/signup',
      templateUrl: 'templates/signup.html',
      controller: 'signupCtrl'
        
    })

    .state('app.pickup', {
      url: '/pickup',
       views: {
        'menuContent': {
      templateUrl: 'templates/pickup.html',
      controller: 'pickupCtrl'
        }
      }
    })
    .state('app.dropoff', {
      url: '/dropoff',
       views: {
        'menuContent': {
      templateUrl: 'templates/dropoff.html',
      controller: 'dropoffCtrl'
        }
      }
    })

     .state('app.review', {
      url: '/review',
       views: {
        'menuContent': {
      templateUrl: 'templates/reviewOrder.html',
      controller: 'reviewCtrl'
        }
      }
    })

     .state('app.complete', {
      url: '/complete',
       views: {
        'menuContent': {
      templateUrl: 'templates/completeOrder.html',
      controller: 'completeCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/landing');
})




.factory(
            "transformRequestAsFormPost",
            function() {
                // I prepare the request data for the form post.
                function transformRequest( data, getHeaders ) {
                    var headers = getHeaders();
                    headers[ "Content-type" ] = "application/json; charset=utf-8";
                    return( serializeData( data ) );
                }
                // Return the factory value.
                return( transformRequest );
                // ---
                // PRVIATE METHODS.
                // ---
                // I serialize the given Object into a key-value pair string. This
                // method expects an object and will default to the toString() method.
                // --
                // NOTE: This is an atered version of the jQuery.param() method which
                // will serialize a data collection for Form posting.
                // --
                // https://github.com/jquery/jquery/blob/master/src/serialize.js#L45
                function serializeData( data ) {
                    // If this is not an object, defer to native stringification.
                    if ( ! angular.isObject( data ) ) {
                        return( ( data == null ) ? "" : data.toString() );
                    }
                    var buffer = [];
                    // Serialize each key in the object.
                    for ( var name in data ) {
                        if ( ! data.hasOwnProperty( name ) ) {
                            continue;
                        }
                        var value = data[ name ];
                        buffer.push(
                            encodeURIComponent( name ) +
                            "=" +
                            encodeURIComponent( ( value == null ) ? "" : value )
                        );
                    }
                    // Serialize the buffer and clean it up for transportation.
                    var source = buffer
                        .join( "&" )
                        .replace( /%20/g, "+" )
                    ;
                    return( source );
                }
            }
        );
