angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;

  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})



.controller('BrowseCtrl', function($scope, $stateParams , $ionicPopup , $state) {


  $scope.driver = function(){

  $state.go('app.driver');
  console.log("::as:");
}



var myPopup;
  $scope.questionInfo = function($event) {


$event.preventDefault();

$event.stopImmediatePropagation();

    console.log("CLicked");

myPopup = $ionicPopup.show({
    template: '<i ng-click="closePop()" style="color:#B20000;position: absolute;right: 2%;font-size: x-large;"class="icon ion-close"></i><br><h4>DRIVER RENTAL</h4><h>Rent a Professional driver to drive you in your own car anywhere within your city for an hour, a whole day<h>',
    scope: $scope,
    title: ''
    
  });
 
   



  };

  $scope.closePop = function () {

    myPopup.close();

  }
})


.controller('landingCtrl', function($timeout,$state,$scope, $stateParams,$ionicModal ,$ionicPopup ,  $window , $http , transformRequestAsFormPost) {


$ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal1) {
    $scope.modal1 = modal1;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal1.hide();

 $timeout(function() {
      
    document.getElementById("a").style.visibility = "visible";
    document.getElementById("b").style.display = "block";

    }, 500);
  };

  // Open the login modal
  $scope.login1 = function() {
    $scope.modal1.show();
        document.getElementById("a").style.visibility = "hidden";
    document.getElementById("b").style.display = "none";
  };



  $ionicModal.fromTemplateUrl('templates/signup.html', {
    scope: $scope
  }).then(function(modal2) {
    $scope.modal2 = modal2;

  });

  // Triggered in the login modal to close it
  $scope.closeSignUp = function() {
    $scope.modal2.hide();

    $timeout(function() {
      
   
        document.getElementById("a").style.visibility = "visible";
    document.getElementById("b").style.display = "block";

     }, 10);
  };

  // Open the login modal
  $scope.signUp1 = function() {
    $scope.modal2.show();
    document.getElementById("a").style.visibility = "hidden";
    document.getElementById("b").style.display = "none";
  };


   $scope.signUp= function ( ) {

var link2 = 'http://dev.mylivecalls.co/aa/bcd/index.php/bcd/register/';

var e_mail =document.getElementById("e_mail").value;
   var mob=         document.getElementById("mob").value;
     var psw =     document.getElementById("psw").value;



        return $http({method : "POST",
     url : link2,
     data : {
            email: e_mail,
            phone: mob,
            password: psw  },
     transformRequest: transformRequestAsFormPost,
     headers : {'Content-Type': "application/x-www-form-urlencoded"}
     //headers : {'Accept' : 'application/json'}
   }).success(function(data) {
     console.log("audits.data" + JSON.stringify(data));

     $scope.closeSignUp();


     if(data.status == "failure")
     {


     }
     else {

      console.log("success");
       
}
     
     

     }).error(function(){

      $scope.closeSignUp();


   console.log("error");

 return null;

});

   };


   var myPopup;
  $scope.questionInfo = function($event) {




    console.log("CLicked");

myPopup = $ionicPopup.show({
    template: '<h5>Error Login<i ng-click="closePop()" style="color:#B20000;position: absolute;right: 2%;;"class="icon ion-close"></i></h5><h>Please provide right credentials<h>',
    scope: $scope,
    title: ''
    
  });

};


 $scope.closePop = function () {

    myPopup.close();

  };


    $scope.login = function ( ) {

var link2 = 'http://dev.mylivecalls.co/aa/bcd/bcd/login';

var e_mail2 =document.getElementById("e_mail2").value;
  
     var psw2 =     document.getElementById("psw2").value;



        return $http({method : "POST",
     url : link2,
     data : {
            email: e_mail2,
            password: psw2  },
     transformRequest: transformRequestAsFormPost,
     headers : {'Content-Type': "application/x-www-form-urlencoded"}
     //headers : {'Accept' : 'application/json'}
   }).success(function(data) {
     console.log("data " + JSON.stringify(data));

    


     if(data.status == "success")
     {
   console.log("success");
       $scope.closeLogin();
       $state.go('app.browse');
       

     }
     else {

$scope.questionInfo();

}
     
     

     }).error(function(){

     


   console.log("error");

 return null;

});




    };








  
})

.controller('pickupCtrl', function($scope, $stateParams ,$state) {


  $scope.dropOff = function () {


if(document.getElementById('address1').value != ""){

   window.localStorage['pickLoc'] = document.getElementById('address1').value ;
  $state.go('app.dropoff')
}

else{
  alert("Select Location");
}
    
  }


  
})



.controller('dropoffCtrl', function($scope, $stateParams,$state) {


 $scope.reviewOrder = function () {
if(document.getElementById('address2').value != ""){

   window.localStorage['dropLoc'] = document.getElementById('address2').value ;
    $state.go('app.review')
  }
else{
  alert("Select Location");
}

  }


  
})


.controller('reviewCtrl', function($scope, $stateParams ,$state,  $ionicModal,$rootScope) {








$rootScope.$on('codesUpdated', function(event, codes,code2 , code3){
  $scope.today2  = codes;
  $scope.Today = code2;
  $scope.time2 = code3; //Rely on localStorage for "storage" only, not for communication.
});


$rootScope.$on('codesUpdated2', function(event, dur){
  $scope.duration  = dur;
  //Rely on localStorage for "storage" only, not for communication.
});
  $scope.today2 = window.localStorage['today']
  $scope.Today = window.localStorage['date']

 $scope.time2  = window.localStorage['time']

$scope.duration = window.localStorage['duration'];


$scope.pick = window.localStorage['pickLoc'];
$scope.drop = window.localStorage['dropLoc'];

          
            










    // Create the login modal 
  $ionicModal.fromTemplateUrl('templates/editDateMod.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;

  });

  // Triggered in the login modal to close it
  $scope.closeEdit = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.openEdit = function() {
    $scope.modal.show();

  };


  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/editDuration.html', {
    scope: $scope
  }).then(function(modal1) {
    $scope.modal1 = modal1;

  });

  // Triggered in the login modal to close it
  $scope.closeDur = function() {
    $scope.modal1.hide();
  };

  // Open the login modal
  $scope.openDur = function() {
    $scope.modal1.show();
    
  };

 

  
})

.directive('fadeOnChange', function($animate) {
  return {
    restrict: 'E',
 
    //We output the value which will be set between fades
    template: '{{theFadedValue.nv}}',
 
    link: function(scope, elem, attr) {
      //The scope variable we will watch
      var vtw = attr.valueToWatch;
 
      //We add the anim class to set transitions
      elem.addClass('anim');
 
      //We watch the value 
      scope.$watch(vtw, function(nv) {
 
        //we fade out
        var promise = $animate.addClass(elem, 'fade-it-out');
 
        //when fade out is done, we set the new value
        promise.then(function() {
 
          scope.$evalAsync(function() {
            //use of dot notation for good practice
            scope.theFadedValue = {
              "nv": nv
            };
            //we fade it back in
            $animate.removeClass(elem, 'fade-it-out');
          });
        });
      })
    }
  };
})
.directive('fadeOnChangeSecond', function($animate) {
  return {
    restrict: 'E',
 
    //We output the value which will be set between fades
    template: '{{theFadedValue1.nv}}',
 
    link: function(scope, elem, attr) {
      //The scope variable we will watch
      var vtw1 = attr.valueToWatch;
 
      //We add the anim class to set transitions
      elem.addClass('anim');
 
      //We watch the value 
      scope.$watch(vtw1, function(nv) {
 
        //we fade out
        var promise = $animate.addClass(elem, 'fade-it-out');
 
        //when fade out is done, we set the new value
        promise.then(function() {
 
          scope.$evalAsync(function() {
            //use of dot notation for good practice
            scope.theFadedValue1 = {
              "nv": nv
            };
            //we fade it back in
            $animate.removeClass(elem, 'fade-it-out');
          });
        });
      })
    }
  };
})

.directive('fadeOnChangeThird', function($animate) {
  return {
    restrict: 'E',
 
    //We output the value which will be set between fades
    template: '{{theFadedValue2.nv}}',
 
    link: function(scope, elem, attr) {
      //The scope variable we will watch
      var vtw2 = attr.valueToWatch;
 
      //We add the anim class to set transitions
      elem.addClass('anim');
 
      //We watch the value 
      scope.$watch(vtw2, function(nv) {
 
        //we fade out
        var promise = $animate.addClass(elem, 'fade-it-out');
 
        //when fade out is done, we set the new value
        promise.then(function() {
 
          scope.$evalAsync(function() {
            //use of dot notation for good practice
            scope.theFadedValue2 = {
              "nv": nv
            };
            //we fade it back in
            $animate.removeClass(elem, 'fade-it-out');
          });
        });
      })
    }
  };
})

.controller('MapController', function($scope, $cordovaGeolocation ,$ionicLoading ) {
     
//var lat1;
//var lng1;
      var posOptions = {
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 0
        };

      $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
           

         

var lat1 = position.coords.latitude   ;  
   var lng1 = position.coords.longitude  ;
     console.log(lat1 + "  s dfdsfds " + lng1);

     //document.getElementById('address').value = lat1 + " " +lng1;

     alert( 'Current Location : ' + '\n' +'Latitude: '          + position.coords.latitude          + '\n' +
          'Longitude: '         + position.coords.longitude         + '\n' 
          );

 

  var geocoder,
  map,
 
    
  markers = [],
  styles = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2e5d4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]}];

function initialize( position ) {
  geocoder = new google.maps.Geocoder();
var myLatlng = new google.maps.LatLng(lat1,lng1);
var mapOptions = {
  zoom: 15,
  center: myLatlng,
  mapTypeControl : false
}
var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


// Place a draggable marker on the map
var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    draggable:true,
    title:"Drag me!"
});


google.maps.event.addListener(marker, 'dragend', function() {
    console.log('Drag ended');
    geocodePosition(marker.getPosition());
  });
}

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      console.log(responses[0].formatted_address);
      document.getElementById('address1').value = responses[0].formatted_address;
    } else {
     console.log('Cannot determine address at this location.');
    }
  });
}






google.maps.event.addDomListener(window, 'load', initialize());



  
});

    


})
.controller('MapController2', function($scope, $cordovaGeolocation ,$ionicLoading ) {
     
//var lat1;
//var lng1;
      var posOptions = {
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 0
        };

      $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
           

         

var lat1 = position.coords.latitude   ;  
   var lng1 = position.coords.longitude  ;
     console.log(lat1 + "  s dfdsfds " + lng1);

     //document.getElementById('address').value = lat1 + " " +lng1;

     alert( 'Current Location : ' + '\n' +'Latitude: '          + position.coords.latitude          + '\n' +
          'Longitude: '         + position.coords.longitude         + '\n' 
          );

 

  var geocoder,
  map,
 
    
  markers = [],
  styles = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2e5d4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]}];

function initialize( position ) {
  geocoder = new google.maps.Geocoder();
var myLatlng = new google.maps.LatLng(lat1,lng1);
var mapOptions = {
  zoom: 15,
  center: myLatlng,
  mapTypeControl : false
}
var map = new google.maps.Map(document.getElementById('map-canvas2'), mapOptions);


// Place a draggable marker on the map
var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    draggable:true,
    title:"Drag me!"
});


google.maps.event.addListener(marker, 'dragend', function() {
    console.log('Drag ended');
    geocodePosition(marker.getPosition());
  });
}

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      console.log(responses[0].formatted_address);
      document.getElementById('address2').value = responses[0].formatted_address;
    } else {
     console.log('Cannot determine address at this location.');
    }
  });
}






google.maps.event.addDomListener(window, 'load', initialize());



  
});



})


.controller('completeCtrl', function($scope, $stateParams ,$ionicHistory , $http , transformRequestAsFormPost) {

   


$ionicHistory.nextViewOptions({
     disableBack: true
});



$scope.submitOrder = function() {


var link2 = 'http://dev.mylivecalls.co/aa/bcd/index.php/bcd/submitorder/';


$scope.today2 = window.localStorage['today']
  $scope.Today = window.localStorage['date']

 $scope.time2  = window.localStorage['time']

$scope.duration = window.localStorage['duration'];


$scope.pick = window.localStorage['pickLoc'];
$scope.drop = window.localStorage['dropLoc'];



        return $http({method : "POST",
     url : link2,
     data : {
        
           pick_up_date : $scope.Today ,
            pick_up_time : $scope.time2  ,
            rent_duration : $scope.duration ,
           pickup_address :  $scope.pick ,
            drop_address  : $scope.drop 


          },
     transformRequest: transformRequestAsFormPost,
     headers : {'Content-Type': "application/x-www-form-urlencoded"}
     //headers : {'Accept' : 'application/json'}
   }).success(function(data) {
     console.log("audits.data" + JSON.stringify(data));

     

     if(data.status == "failure")
     {


     }
     else {

      console.log("success");
       
}
     
     

     }).error(function(){

   


   console.log("error");

 return null;

});

   

}



})

.controller('signupCtrl', function($scope, $stateParams ) {

   







})


.controller('driverCtrl', function($scope, $stateParams,$state ) {



   

$scope.info = function()
{

  $state.go('app.info');
}





})


.controller('infoCtrl', function($scope, $stateParams,$state ) {



   
$scope.req = function()
{

  $state.go('app.req');
}






})
.controller('reqCtrl', function($scope, $stateParams,$state ) {





   







})


.controller('rentalCtrl', function($scope, $stateParams , $state , $rootScope) {



///console.log("Date" + moment().format("MMM Do YY"));               // Sep 12th 15 // September 12th 2015, 12:19:17 pm)

$scope.Today = moment().format("MMM DD YYYY"); 

var today = moment();
console.log("Date" + $scope.Today);  
$scope.today2 = "Today" ;           // Sep 12th 15

$scope.addDate = function ()
{




$scope.Today = moment(today).add(1 , 'days').format("MMM DD YYYY");      // Tomorrow at 12:23 PM

today = $scope.Today;

$scope.today2 =  moment(today).format('ddd');

console.log("Date" + $scope.Today);  

console.log($scope.today2);



}

$scope.subDate = function ()
{




$scope.Today = moment(today).add(-1 , 'days').format("MMM DD YYYY"); ;       // Tomorrow at 12:23 PM

today = $scope.Today;

console.log("Date" + $scope.Today);  

$scope.today2 =  moment(today).format('ddd');



}

$scope.time = moment({h: 10, m:30});



console.log($scope.time);

$scope.time2 = $scope.time.format("hh:mm") ;


$scope.addTime = function()
{

  $scope.time  = moment($scope.time).add(05, 'minutes'); 

  $scope.time2 = $scope.time.format("hh:mm");



  console.log("time =" + $scope.time2);

}

$scope.subTime = function()
{

  $scope.time  = moment($scope.time).subtract(05, 'minutes'); 

  $scope.time2 = $scope.time.format("hh:mm");



  console.log("time =" + $scope.time2);

}



$scope.duration= 1;
$scope.durationhr = $scope.duration + " Hours"


$scope.addDur = function(){

if($scope.duration <= 7){

$scope.duration= $scope.duration + 1;

$scope.durationhr = $scope.duration + " Hours";

}

else  if($scope.duration == 8 ){

$scope.duration= $scope.duration + 1;
$scope.durationhr= "All Day";


}

else  if($scope.duration == 9 ){
$scope.duration= $scope.duration + 1;
$scope.durationhr= "All Week";


}

}

$scope.subDur = function(){

if($scope.duration > 1 ){
$scope.duration= $scope.duration - 1;
$scope.durationhr = $scope.duration + " Hours";
}

else  if($scope.duration == 8  ){

$scope.duration= $scope.duration - 1;
$scope.durationhr= "All Day";


}

else  if($scope.duration == 9  ){
$scope.duration= $scope.duration - 1;
$scope.durationhr= "All Week";


}

}


$scope.pickup = function ()
{
 
 



window.localStorage['today'] = $scope.today2;
window.localStorage['date'] = $scope.Today;
window.localStorage['time'] =  $scope.time2 ;
window.localStorage['duration'] =  $scope.durationhr;
 
console.log($scope.today2 + " " + $scope.Today + " " + $scope.time2 + "" + $scope.durationhr);

  $state.go('app.pickup');
}

$scope.editDateTime = function ()
{
 
 



window.localStorage['today'] = $scope.today2;
window.localStorage['date'] = $scope.Today;

 
console.log($scope.today2 + " " + $scope.Today + " " + $scope.time2 + "" + $scope.durationhr);


// <That part's up to you
$rootScope.$emit('codesUpdated', $scope.today2 ,$scope.Today , $scope.time2);


  $scope.closeEdit();

}
$scope.editDur = function ()
{
 
 



window.localStorage['duration'] = $scope.durationhr;


 
console.log($scope.today2 + " " + $scope.Today + " " + $scope.time2 + "" + $scope.durationhr);


// <That part's up to you
$rootScope.$emit('codesUpdated2', $scope.durationhr);


  $scope.closeDur();

}

});




